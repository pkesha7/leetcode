package medium;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Given an m x n 2D binary grid which represents a map of '1's (land) and '0's (water), return
 * the number of islands.
 *
 * <p>An island is surrounded by water and is formed by connecting adjacent lands horizontally or
 * vertically. You may assume all four edges of the grid are all surrounded by water.
 *
 * <p>Input: grid = [
 * ["1","1","1","1","0"],
 * ["1","1","0","1","0"],
 * ["1","1","0","0","1"],
 * ["0","0","0","0","0"]]
 * Output: 1
 *
 * <p>Input: grid = [
 * ["1","1","0","0","0"],
 * ["1","1","0","0","0"],
 * ["0","0","1","0","0"],
 * ["0","0","0","1","1"]]
 * Output: 3
 */
public final class NumberOfIslands {
  private static int mRow;
  private static int nColumn;
  private static final Set<String> visited = new HashSet<String>();
  private static final int[][] neighbors = {{-1,0}, {0,-1}, {0,1}, {1,0}};

  public final static void main(String[] args) {

    //    final int[][] grid =
    //        new int[][] {
    //          {1, 1, 0, 1, 0},
    //          {1, 1, 0, 1, 0},
    //          {1, 1, 0, 0, 0},
    //          {0, 0, 0, 0, 1}
    //        };

    final int[][] grid =
        new int[][] {
          {1, 0, 1},
          {0, 0, 0},
          {1, 1, 1}
        };

    int island = 0;
    mRow = grid.length;
    nColumn = grid[0].length;

    for (int i = 0; i < mRow; i++) {
      for (int j = 0; j < nColumn; j++) {
        final boolean newIsland = (grid[i][j] == 1) && !visited.contains(i + ":" + j);
        island += newIsland ? 1 : 0;
        visited.add(i + ":" + j);
        bfs(grid, i, j);
      }
    }

    System.out.println(island);
  }

  private final static void bfs(final int[][] grid, final int row, final int col) {
    // checkNeighbors will store lands connected to the current island
    final Queue<int[]> checkNeighbors = new LinkedList<int[]>();
    checkNeighbors.add(new int[] {row, col});

    // This will then search all lands connected to a piece of land and stored as visited
    // The connected lands are an island, and will be added checkNeighbors
    while (!checkNeighbors.isEmpty()) {
      // Removes head of the poll - store it into current for check
      final int[] current = checkNeighbors.poll();

      // For all the neighbors
      for (final int[] neighbor : neighbors) {
        final int lookAheadRow = current[0] + neighbor[0];
        final int lookAheadColumn = current[0] + neighbor[1];

        // Checking bounds and is island since we're first using indexes, not values
        final boolean lookAheadRowValid = ((lookAheadRow >= 0) && (lookAheadRow < mRow));
        final boolean lookAheadColumnValid = ((lookAheadColumn >= 0) && (lookAheadColumn < nColumn));
        if (lookAheadRowValid && lookAheadColumnValid) {

          final boolean currentIsIsland  = (grid[lookAheadRow][lookAheadColumn] == 1);

          // Conditions to check to add
          final boolean shouldAdd = currentIsIsland && !visited.contains(lookAheadRow + ":" + lookAheadColumn);

          if (shouldAdd) {
            // Will add look ahead row to check in the queue
            checkNeighbors.add(new int[] {lookAheadRow, lookAheadColumn});
            visited.add(lookAheadRow + ":" + lookAheadColumn);
          } else  {
            continue;
          }
        } else {
          continue;
        }





      }
    }
  }
}
