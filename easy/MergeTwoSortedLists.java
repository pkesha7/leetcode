package easy;

import java.util.Objects;

public class MergeTwoSortedLists {
  /*
  * You are given the heads of two sorted linked lists list1 and list2.
  * Merge the two lists into one sorted list. The list should be made by splicing together the nodes of the first two lists.
  * Return the head of the merged linked list.
  *
  * Constraints:
  * The number of nodes in both lists is in the range [0, 50].
  * -100 <= Node.val <= 100
  * Both list1 and list2 are sorted in non-decreasing order.
  *
  *
  */

  public static void main(String[] args){
     ListNode list1 = new ListNode();
     ListNode list2 = new ListNode();

     // Both refer to the created object
     // Stores the list
     final ListNode sorted = new ListNode();
     // Points to current head
     ListNode current = sorted;

    while (Objects.nonNull(list1) && Objects.nonNull(list2)) {
      if (list1.val >= list2.val) {
        current.next = list2;
        list2 = list2.next;
      } else {
        current.next = list1;
        list1 = list1.next;
      }
      current = current.next;
    }

    if (Objects.nonNull(list1)) {
      current.next = list1;
    }
    if (Objects.nonNull(list2)) {
      current.next = list2;
    }
    System.out.println(sorted);
  }

  /**
   * Definition for singly-linked list.
   */
   private static class ListNode {
     int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
   }

  class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
      ListNode dummy = new ListNode();
      ListNode cur = dummy;

      while (list1 != null && list2 != null) {
        if (list1.val > list2.val) {
          cur.next = list2;
          list2 = list2.next;
        } else {
          cur.next = list1;
          list1 = list1.next;
        }
        cur = cur.next;
      }

      cur.next = (list1 != null) ? list1 : list2;

      return dummy.next;
    }
  }
}
