package easy;/*
* Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
* You may assume that each input would have exactly one solution, and you may not use the same element twice.
* You can return the answer in any order.
*
* Constraints:
* 2 <= nums.length <= 104
* -109 <= nums[i] <= 109
* -109 <= target <= 109
* Only one valid answer exists.
*
*/

import java.util.HashMap;
import java.util.Map;

public class TwoSum {

  public static void main(String[] args){

    final int[] arr = {2, 7, 11, 15};
    final int target = 9;
    final Map<Integer, Integer> map = new HashMap<>();

    for (int i = 0; i < arr.length; i++){
      map.put(arr[i], i);
    }

    for (int i = 0; i < arr.length; i++) {
      // Make sure the difference is in the correct order
      final int diff = target - arr[i];
      if (map.containsKey(diff)) {
        final int[] index = {i, map.get(diff)};
        System.out.println(index[0] + " " + index[1]);
        break;
      } else {
        continue;
      }
    }
  }
}
