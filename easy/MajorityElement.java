package easy;

import java.util.HashMap;
import java.util.Map;

public class MajorityElement {
  /*
  * Given an array nums of size n, return the majority element.
  * The majority element is the element that appears more than ⌊n / 2⌋ times.
  * You may assume that the majority element always exists in the array.
  */
  public static void main(String[] args) {
    final int[] nums = {2,2,1,1,1,2,2};
    final int majorityFrequency = nums.length/2;
    Map<Integer, Integer> map = new HashMap<Integer, Integer>();

    for (int num : nums) {
      map.put(num, map.getOrDefault(num, 0) + 1);
      if (map.get(num) > majorityFrequency) {
        System.out.println(num);
        break;
      }
    }
  }
}
